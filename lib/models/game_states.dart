
import 'package:flutter/material.dart';

class MyGameState extends ChangeNotifier {
  int value = 0;
  int throws = 0;
  bool blind = false;
  bool done = false;
  List<int> wuerfel = [0, 0, 0];
  List<bool> fixed = [false, false, false];
  List<bool> hidden = [true, true, true];
  int coins = 0;

  void update(Map<String, dynamic> data) {
    value = data['value'];
    throws = data['throws'];
    blind = data['blind'];
    done = data['done'];
    wuerfel = data['wuerfel'];
    coins = data['coins'];
    notifyListeners();
  }
}


class TheirGameStates extends ChangeNotifier {
  // Keeps track of all the game states for all the players
  Map<String, MyGameState> gameStates = {};

  void update(Map<String, dynamic> data) {
    if (gameStates.containsKey(data['player_id'])) {
      gameStates[data['player_id']]?.update(data);
    } else {
      gameStates[data['player_id']] = MyGameState();
      gameStates[data['player_id']]?.update(data);
    }
    notifyListeners();
  }
}