import 'package:schocken/domain/profile_controls.dart';
import 'package:schocken/domain/schocken.dart';
import 'package:flutter/material.dart';

class MyGameControlsWidget extends StatefulWidget {
  // MyGameControlsWidget({Key key}) : super(key: key);
  final GameStateTracker game;
  const MyGameControlsWidget(
      {super.key, required this.game, required this.playersStream});
  final Stream<List<Map<String, dynamic>>> playersStream;

  @override
  MyGameControlsWidgetState createState() => MyGameControlsWidgetState();
}

class MyGameControlsWidgetState extends State<MyGameControlsWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: MySchockenWidget(
        game: widget.game,
        playersStream: widget.playersStream,
      ),
    );
  }
}
