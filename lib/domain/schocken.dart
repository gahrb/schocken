import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:schocken/const.dart';
import 'package:schocken/domain/profile_controls.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class MySchockenWidget extends StatefulWidget {
  const MySchockenWidget(
      {super.key, required this.game, required this.playersStream});
  final GameStateTracker game;
  final Stream<List<Map<String, dynamic>>> playersStream;
  @override
  MySchockenWidgetState createState() => MySchockenWidgetState();
}

class MySchockenWidgetState extends State<MySchockenWidget> {
  var rnd = Random();
  late List<int> wuerfel;
  late List<bool> fixed, locked, cupHide, tableHide;
  late Stream<List<Map<String, dynamic>>> gameStream;

  late int numThrows, maxThrows, coins, finalizedPlayers, totalPlayers;
  late bool wurfWillig,
      throwReady,
      endAble,
      muetze,
      blindAble,
      blind,
      finalized,
      starter;
  Map<String, dynamic> _startingPlayerState = {};
  late List<dynamic> _players;

  bool public = false;

  void initRoll() {
    setState(() {
      // New Round! We reset the values.
      // give the dices a new value at the start
      wuerfel = [rnd.nextInt(6) + 1, rnd.nextInt(6) + 1, rnd.nextInt(6) + 1];
      cupHide = [true, true, true];
      tableHide = [true, true, true];
      fixed = [false, false, false];
      locked = [false, false, false];
      wurfWillig = true;
      throwReady = false;
      endAble = false;
      finalized = false;
      blindAble = false;
      blind = false;
      numThrows = 0;
      public = false;
      muetze = true;
      coins = 0;
      finalizedPlayers = 0;
      starter = false;
      // "Online" Variables
      maxThrows = 0;
    });
  }

  void blindCall() {
    // A player makes a blind wurf
    if (numThrows == 0) {
      setState(() {
        // This only has to be set true, because afterwarts genug() is called --> new round starts and it's false again
        blind = true;
        finalized = true;
      });
      wurf();
    } else {
      setState(() {
        blind = false;
      });
    }
    genug();
  }

  void wurf() {
    // A player makes a normal wurf
    if (!fixed.any((f) => f)) {
      setState(() {
        // Check for muetzenwurf
        muetze = true;
      });
    } else {
      setState(() {
        muetze = false;
      });
    }

    for (int i = 0; i < 3; i++) {
      if (!fixed[i]) {
        setState(() {
          wuerfel[i] = rnd.nextInt(6) + 1;
          cupHide[i] = false;
          fixed[i] = true;
          locked[i] = false;
        });
      }
    }
    setState(() {
      numThrows += 1;
      throwReady = false;
      public = false;
      endAble = true;
      blindAble = false;
    });
    updateGameState();
    getGameState();
  }

  genug() {
    if (kDebugMode) {
      print("We are done with rolling.");
    }
    setState(() {
      finalized = true;
      for (int i = 0; i < 3; i++) {
        fixed[i] = true;
      }
    });
    updateGameState();
    getOthersValues();
  }

  reveal() {
    if (kDebugMode) {
      print("Revealing our hand.");
    }
    setState(() {
      for (int i = 0; i < 3; i++) {
        cupHide[i] = false;
        tableHide[i] = false;
      }
    });
    updateGameState();
  }

  void neuWurf(int i, {bool swap = false}) {
    // Change the fixed state for each dice.
    if (wurfable()) {
      setState(() {
        if (swap) {
          fixed[i] = false;
          locked[i] = true;
        } else if (!locked[i]) {
          fixed[i] = !fixed[i];
        }
        if (fixed.any((f) => !f) || cupHide.any((h) => h)) {
          wurfWillig = true;
          endAble = false;
        } else {
          wurfWillig = false;
          endAble = true;
        }
      });
    }
    getGameState();
  }

  void swap6s(int swapIdx) {
    // Two 6s may be changed into one 1, the other dice must be rethrown
    // --> can only be done when one roll is left
    if (numThrows < maxThrows && wuerfel[swapIdx] == 6) {
      // Get all Dices with a 6
      int sechsen = 0;
      for (var w in wuerfel) {
        if (w == 6) {
          sechsen++;
        }
      }
      // Only perform swap if more than two 6s are on the table
      if (sechsen > 1) {
        int rethrowIdx = 0;
        for (int i = 0; i < 3; i++) {
          if (i != swapIdx && wuerfel[i] == 6) {
            // Rethrow the other dice
            rethrowIdx = i;
            // Mark this dice to be rethrown --> Hide it
            neuWurf(i, swap: true);
            break;
          }
        }

        setState(() {
          // Hide the other dice. It has to be rethrown
          cupHide[rethrowIdx] = true;
          tableHide[rethrowIdx] = true;
          // Fix the onHold Dice
          wuerfel[swapIdx] = 1;
          fixed[swapIdx] = true;
          cupHide[swapIdx] = false;
          tableHide[swapIdx] = false;
        });
      }
    }
    updateGameState();
    getGameState();
  }

  void offTable() {
    for (int i = 0; i < 3; i++) {
      if (!fixed[i]) {
        setState(() {
          cupHide[i] = true;
          tableHide[i] = true;
        });
      } else {
        tableHide[i] = false;
      }
    }
    setState(() {
      throwReady = true;
      wurfWillig = false;
    });
  }

  void setStartingUserState() async {
    String startingPlayer = await widget.game.getStartingUser();
    if (kDebugMode) {
      print("Starting player is $startingPlayer");
    }
    var startingPlayerState =
        await widget.game.getPlayerState(playerId: startingPlayer);
    setState(() {
      startingPlayer = startingPlayer;
      _startingPlayerState = startingPlayerState.last as Map<String, dynamic>;
      maxThrows = _startingPlayerState['throws'];
    });
  }

  bool wurfable() {
    if (numThrows >= maxThrows || finalized) {
      return false;
    }
    return true;
  }

  bool isEndable() {
    if (!cupHide.any((h) => !h) ||
        (cupHide.every((h) => h) && numThrows < maxThrows)) {
      return true;
    }
    return false;
  }

  void getGameState() async {
    // Messages Abfragen für z.b.
    // max_roll, loose, win

    // when createor joins set 'gameStarted', then fix the list of players
    var remotePlayers = await widget.game.joinedPlayers();
    setState(() {
      _players = remotePlayers;
    });

    // Check if we are the first to play --> we decide how often to roll..
    if (starter) {
      if (kDebugMode) {
        print("We are starter");
      }
      setState(() {
        maxThrows = 3;
        if (numThrows == 0) {
          // We can roll blind! YAY!
          blindAble = true;
        }
      });
    } else {
      // Check how many throws the starter has --> this is our maxThrows
      setStartingUserState();
      setState(() {
        maxThrows = 1;
        numThrows = 0;
      });
    }
  }

  Future<void> getOthersValues() async {
    // Only continue, if necessary --> this shouldn't happen
    if (!finalized) {
      return;
    }
    // Check if everybody is done & rank all players.
    setState(() {
      finalizedPlayers = 0;
    });
    const Duration updateInverval = Duration(seconds: 2, milliseconds: 14);
    if (kDebugMode) {
      print("Start checking the other players.");
    }
    for (String player in _players) {
      Map<String, dynamic> playerState = {};
      bool playerDone = false;

      Timer.periodic(updateInverval, (timer) async {
        playerState = (await widget.game.getPlayerState(playerId: player)).last;
        if (kDebugMode) {
          print("Updating playerState: $player: $playerState");
        }
        playerDone = playerState.containsKey('done') && playerState['done'];

        if (playerDone) {
          if (kDebugMode) {
            print("Player '$player' is done.");
          }
          setState(() {
            finalizedPlayers++;
          });
          timer.cancel();
        }
      });
    }
  }

  Future<List<int>> finishUpLeg(
      {required Map<String, int> othersCoins,
      required Map<String, int> othersValues}) async {
    // If we are here, everybody is done --> we can calculate the winner and loser
    // Calculate winner and loser
    Map<String, String> winnerAndLoser =
        getWinnerAndLoser(throwValues: othersValues);
    int potCoins =
        max(0, 13 - othersCoins.values.reduce((sum, value) => sum + value));

    if (kDebugMode) {
      print("Others values: $othersValues");
      print("Others coins: $othersCoins");
      print("Pot coins: $potCoins");
    }

    int loserWinner = 0;
    // ARE WE THE LOSER?!?
    if (winnerAndLoser['loser'] == widget.game.user.id) {
      int maxCoins = getCoins(othersValues[winnerAndLoser['winner']]!);
      // Check if the throw was blind:
      var playerState = (await widget.game
              .getPlayerState(playerId: winnerAndLoser['winner']!))
          .last;

      // For race condition reasons, we get also our coins from the DB
      Map<String, dynamic> ourState =
          (await widget.game.getPlayerState(playerId: widget.game.user.id))
              .last;

      if (playerState['blind'].toString() == "true") {
        maxCoins = 2 * maxCoins;
      }
      if (kDebugMode) {
        print("We Take: $maxCoins");
      }

      loserWinner = -1;
      if (maxCoins == 13) {
        coins = -ourState['coins'];
      } else {
        int coinsToTake = 0;
        // Take from the pot if possible
        if (potCoins > 0) {
          coinsToTake = min(potCoins, maxCoins);
        } else {
          coinsToTake = min(playerState['coins'], maxCoins);
        }
        coins = ourState['coins'] + coinsToTake;
      }
    }

    // ARE WE THE WINNNER?
    if (winnerAndLoser['winner'] == widget.game.user.id) {
      Map<String, dynamic> ourState =
          (await widget.game.getPlayerState(playerId: widget.game.user.id))
              .last;
      // CHECK HOW MUCH WE HAVE TO GIVE
      int maxCoins = getCoins(calcValue(
          wuerfel: List<int>.from(ourState['wuerfel']),
          muetze: ourState['muetze'],
          throws: ourState['throws']));
      // Check if the throw was blind:
      if (ourState['blind']) {
        maxCoins = 2 * maxCoins;
      }
      if (kDebugMode) {
        print("They get: $maxCoins");
      }

      loserWinner = 1;
      if (maxCoins == 13) {
        coins = -ourState['coins'];
      } else {
        int coinsToGive = 0;
        int potCoins =
            max(0, 13 - othersCoins.values.reduce((sum, value) => sum + value));
        // Take from the pot if possible
        if (potCoins == 0) {
          coinsToGive = min(ourState['coins'], maxCoins);
          if (kDebugMode) {
            print("...from us.");
          }
        }
        coins = ourState['coins'] - coinsToGive;
      }
    }

    return [loserWinner, coins];
  }

  void updateGameState() async {
    // Prepare the Map to upload
    List<int> wuerfelUpload = [0, 0, 0];
    Map<String, dynamic> data = {
      'blind': blind,
      'done': finalized,
      'throws': numThrows,
      'wuerfel': wuerfelUpload,
      'muetze': muetze,
      'coins': coins,
      'value': 0
    };
    for (int i = 0; i < wuerfel.length; i++) {
      wuerfelUpload[i] = !tableHide[i] && fixed[i] ? wuerfel[i] : 0;
    }
    data.update('wuerfel', (value) => wuerfelUpload);
    data.update(
        'value',
        (value) => calcValue(
            wuerfel: wuerfelUpload, muetze: muetze, throws: numThrows));
    if (kDebugMode) {
      print("SENDING DATA: $data");
    }
    widget.game.sendPlayerState(data: data);
    getGameState();
  }

  @override
  initState() {
    initRoll();
    super.initState();
    updateGameState();
    getGameState();
  }

  @override
  Widget build(BuildContext context) {
    double width = min(MediaQuery.of(context).size.width, 600);

    if (finalized) {
      if (!tableHide.any((element) => element)) {
        return StreamBuilder(
          stream: widget.playersStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }

            if (snapshot.hasError || snapshot.data == null) {
              return const Text("Loading Data...");
            }

            return waitingForPlayerEndWidget(
              context,
              widget.game.getPlayersGameStateStream(
                  playersId: List<String>.from(snapshot.data!.last["players"])),
            );
          },
        );
      }
      return GestureDetector(
          onTap: () {
            reveal();
          },
          child: Image.asset(
            "assets/icons/zu.png",
            fit: BoxFit.contain,
          ));
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Text("Wuerfe: $numThrows/$maxThrows"),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              wuerfelWidget(0, width),
              wuerfelWidget(1, width),
              wuerfelWidget(2, width),
            ],
          ),
        ]),
        wurfWillig
            ? Expanded(
                // your image goes here which will take as much height as possible.
                child: GestureDetector(
                    onTap: () {
                      setState(() {
                        finalized = false;
                      });
                      offTable();
                      wurf();
                    },
                    onLongPress: () {
                      // TODO: add vibrating stuff
                      setState(() {
                        finalized = false;
                      });
                      offTable();
                      blindCall();
                    },
                    child: Image.asset("assets/icons/open.png",
                        fit: BoxFit.contain)),
              )
            : Expanded(
                child: !finalized && ((endAble) || (throwReady && blindAble))
                    ? GestureDetector(
                        onTap: () {
                          !finalized ? blindCall() : genug();
                        },
                        child: Image.asset(
                          "assets/icons/zu.png",
                          fit: BoxFit.contain,
                        ))
                    : Container(),
              )
      ],
    );
  }

  Widget waitingForPlayerEndWidget(
      BuildContext context, SupabaseStreamBuilder playersStream) {
    return StreamBuilder(
        stream: playersStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }
          if (snapshot.data == null || snapshot.data.length == 0) {
            return const Text("Waiting for Round to finish...");
          }
          List<Map<String, dynamic>> playersStates = snapshot.data!;
          if (kDebugMode) {
            print("PlayersStates:$playersStates");
          }
          Map<String, int> othersValues = {};
          Map<String, int> othersCoins = {};
          // TODO: We are done. Hence we set the starting player to unkown.
          // Once everyone has revealed their hand. We query the starter player and
          // take the moment it has been set as the beacon to start the new round.
          // setState({
          // startingPlayer = "unknown";
          // });

          bool allRevealed = !playersStates.any((value) {
            if (kDebugMode) {
              print(
                  "Player: ${value['player_id']} value: ${value['dice_state'].last}");
            }
            bool revealed = !List<int>.from(value['dice_state'].last['wuerfel'])
                .any((wuerfel) => wuerfel == 0);

            if (revealed) {
              if (kDebugMode) {
                print("Player: ${value['player_id']} has revealed.");
              }
              othersValues.putIfAbsent(
                  value['player_id'], () => value['dice_state'].last['value']);
              othersCoins.putIfAbsent(
                  value['player_id'], () => value['dice_state'].last['coins']);
            }
            return !revealed;
          });
          if (allRevealed) {
            if (kDebugMode) {
              print("All players are done and have opened their cup.");
            }
            finishUpLeg(othersCoins: othersCoins, othersValues: othersValues)
                .then((value) {
              // Update the DB
              if (kDebugMode) {
                print("Updating gameState after waiting update Interval");
              }
              Future.delayed(const Duration(seconds: 1, milliseconds: 420) * 2,
                  () {
                setState(() {
                  starter = value[0] == -1;
                  coins = value[1];
                  wuerfel = [
                    rnd.nextInt(6) + 1,
                    rnd.nextInt(6) + 1,
                    rnd.nextInt(6) + 1
                  ];
                  cupHide = [true, true, true];
                  tableHide = [true, true, true];
                  fixed = [false, false, false];
                  locked = [false, false, false];
                  wurfWillig = true;
                  throwReady = false;
                  endAble = false;
                  blindAble = value[0] == -1;
                  blind = false;
                  numThrows = 0;
                  public = false;
                  muetze = true;
                  // "Online" Variables
                  maxThrows = 0;
                  finalized = false;
                });
                if (value[0] == -1) {
                  widget.game
                      .setGameState({'starting_user': widget.game.user.id});
                } // Populate the starting user
                updateGameState();
              });
            });
          }
          return const Text("Waiting for all others to open their cup.");
        });
  }

  Widget wuerfelWidget(int i, double width) {
    double len = width / 8;
    return GestureDetector(
      onTap: () {
        if (numThrows > 0) {
          neuWurf(i);
        }
      },
      onLongPress: () {
        if (numThrows > 0) {
          swap6s(i);
        }
      },
      child: cupHide[i]
          ? Container(
              height: len,
              width: len,
              decoration: BoxDecoration(
                border: Border.all(
                  color: fixed[i] ? certainColor : uncertainColor,
                  width: 3,
                ),
              ),
              child: Image.asset(
                "assets/icons/?.png",
              ),
            )
          : Container(
              height: len,
              width: len,
              decoration: BoxDecoration(
                border: Border.all(
                  color: fixed[i] ? certainColor : uncertainColor,
                  width: 3,
                ),
              ),
              child: Image.asset("assets/icons/${wuerfel[i].toString()}.png"),
            ),
    );
  }

  Widget actionButton(String text, Function func) {
    return TextButton(
      onPressed: func(),
      child: Text(text),
    );
  }
}

Map<String, String> getWinnerAndLoser({
  throwValues = Map<String, int>,
}) {
  // Sort the players by their dice values
  List<String> players = throwValues.keys.toList();
  players.sort((a, b) {
    return -throwValues[a].compareTo(throwValues[b]!); // Sort descending
  });
  // The winner is the first player with the fewest throws and the highest value
  String winner = players.first;
  // The loser is the last player with the most throws and the lowest value
  String loser = players.last;
  return {'winner': winner, 'loser': loser};
}

int getCoins(int score) {
  int coins;
  if (score >= 40000) {
    // schock out
    coins = 13;
  } else if (score >= 36000) {
    // schock 6
    coins = 6;
  } else if (score >= 35000) {
    // schock 5
    coins = 5;
  } else if (score >= 34000) {
    // schock 4
    coins = 4;
  } else if (score >= 33000) {
    // schock 3
    coins = 3;
  } else if (score >= 32000) {
    // schock 2
    coins = 2;
  } else if (score >= 20000) {
    // Drilling
    coins = 3;
  } else if (score >= 10000) {
    // Strasse
    coins = 2;
  } else {
    // Hausnummer
    coins = 1;
  }
  return coins;
}

int calcValue({
  required List<int> wuerfel,
  required bool muetze,
  required int throws,
}) {
  int score;
  // Return 0 if any wuerfel is 0:
  if (wuerfel[0] == 0 || wuerfel[1] == 0 || wuerfel[2] == 0) {
    return 0;
  }

  // order so that w1 <= w2 <= w3
  wuerfel.sort();
  if (wuerfel[0] == 1 && wuerfel[1] == 1 && wuerfel[2] == 1) {
    score = 40000;
  } else if (wuerfel[0] == 1 && wuerfel[1] == 1) {
    score = 30000 + 1000 * wuerfel[2];
  } else if (wuerfel[0] == wuerfel[2]) {
    score = 20000 + 1000 * wuerfel[0];
    // Since the list is in order, this is sufficient.
  } else if (wuerfel[2] - wuerfel[1] == 1 &&
      wuerfel[1] - wuerfel[0] == 1 &&
      muetze) {
    score = 10000 + 1000 * wuerfel[0];
  } else {
    score = 1000 * wuerfel[2] + 100 * wuerfel[1] + 10 * wuerfel[0];
  }
  if (muetze) {
    score = score + 1;
  }
  score = score + 6 - 2 * throws;
  return score;
}
