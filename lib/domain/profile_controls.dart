// This file contains the logic for the profile controls.

import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:crypto/crypto.dart';
import 'package:schocken/const.dart';
import 'dart:convert'; // for the utf8.encode method

import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:uuid/uuid.dart';

Future<String> getLocalId() async {
  return await SharedPreferences.getInstance().then((prefs) {
    if (kDebugMode) {
      print("Getting local id");
    }
    String? localId = prefs.getString('userId');
    if (localId == null) {
      localId = const Uuid().v4();
      prefs.setString('userId', localId);
      if (kDebugMode) {
        print("No local id found, creating new one: $localId");
      }
    } else {
      if (kDebugMode) {
        print("Found local id: $localId");
      }
    }
    return localId;
  });
}

void setLocalId(String localId) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('userId', localId);
}

class SchockenUser {
  final SupabaseClient client;
  late final String userId;
  SchockenUser({required this.client});
  late bool initialized = false;
  late bool loggedIn = false;

  Future<void> init() async {
    userId = await getLocalId();
    if (kDebugMode) {
      print("Starting initialization of SchockenUser...");
      print("Finished initialization of SchockenUser with id: $userId");
    }
    initialized = true;
  }

  Future<void> newProfile() async {
    var response = await client.auth
        .signUp(email: "$userId@gahrb.dev", password: getPasswordHash(userId));
    if (kDebugMode) {
      print("Creating new profile");
      print("Created new user: $response");
    }
  }

  String getPasswordHash(String password) {
    var bytes = utf8.encode(password); // data being hashed
    var digest = sha1.convert(bytes);
    return digest.toString();
  }

  Future<void> loginUser() async {
    try {
      var response = await client.auth.signInWithPassword(
          email: "$userId@gahrb.dev", password: getPasswordHash(userId));
      if (kDebugMode) {
        print("Logged in user: $response");
      }
      // ignore: non_constant_identifier_names
    } on AuthException {
      if (kDebugMode) {
        print("User not found, creating new profile");
      }
      await newProfile();
      await loginUser();
    }
    client.auth.refreshSession();
  }

  bool get isInitialized => initialized;
  String get id => userId;
  String get name => getUserDisplayName(uuid: userId);
}

class GameStateTracker {
  // Syncs the current GameState with Supabase
  final SchockenUser
      user; // This is the user that is currently logged in, NOT the user that created the game
  final SupabaseClient client;
  final String gameId;
  GameStateTracker({required this.client, required this.user, gameId})
      : gameId = gameId ?? const Uuid().v4();

  Future<Map<String, dynamic>> getGameState() async {
    final response =
        await client.from('game_state').select().eq('game_id', gameId);
    return response[0];
  }

  // Same as above, but as a stream
  Stream<List<Map<String, dynamic>>> getGameStateStream() {
    return client
        .from('game_state')
        .stream(primaryKey: ["game_id"]).eq("game_id", gameId);
  }

  Future<void> setGameState(Map<String, dynamic> data) async {
    final response =
        await client.from('game_state').update(data).eq('game_id', gameId);
    if (response != null) {
      if (kDebugMode) {
        print(response);
      }
    }
  }

  Future<bool> getPlayerDone({required String playerId}) async {
    final response = await client
        .from('player_state')
        .select("dice_state")
        .eq("player_id", playerId)
        .eq("game_id", gameId);
    return response.last["done"] ?? false;
  }

  // New game --> creates a new
  Future<dynamic> newGame() async {
    final response = await client.from('game_state').insert({
      'game_id': gameId,
      'starting_user': user.id,
      'players': [user.id],
    }).select();
    if (kDebugMode) {
      print(response);
    }
    return response;
  }

  Future<void> joinGame() async {
    // Query the list of current players from the others game
    // Append to the list of current players
    // Update the list in the database
    if (kDebugMode) {
      print("Joining Game.\nQuerying current state '$gameId'");
    }
    dynamic response =
        await client.from('game_state').select().eq('game_id', gameId);
    if (kDebugMode) {
      print(response);
    }
    var availablePlayers = response[0]['players'];
    if (availablePlayers.contains(user.id)) {
      if (kDebugMode) {
        print("User already in game");
      }
      return;
    }
    response = await client
        .from('game_state')
        .update({
          'players': response[0]['players'] + [user.id],
        })
        .eq('game_id', gameId)
        .select();
    if (kDebugMode) {
      print(response);
    }
  }

  Stream<List<Map<String, dynamic>>> joinedPlayerStream() {
    return client
        .from('game_state')
        .stream(primaryKey: ["players"]).eq("game_id", gameId);
  }

  Future<List<dynamic>> joinedPlayers() async {
    final response = await client
        .from('game_state')
        .select("players")
        .eq("game_id", gameId)
        .then((value) {
      return value.last["players"];
    });
    return response;
  }

  Future<String> getStartingUser() {
    return client
        .from('game_state')
        .select("starting_user")
        .eq("game_id", gameId)
        .then((value) => value.last["starting_user"]);
  }

  Future<dynamic> getPlayerState({required String playerId}) {
    return client
        .from('player_state')
        .select("dice_state")
        .eq("player_id", playerId)
        .then((value) => value.last["dice_state"]);
  }

  SupabaseStreamBuilder getPlayersGameStateStream(
      {required List<String> playersId}) {
    return client
        .from('player_state')
        .stream(primaryKey: ["players"]).inFilter("player_id", playersId);
  }

  Future<void> sendPlayerState({required Map<String, dynamic> data}) async {
    if (kDebugMode) {
      print(
          "Sending player state for user: ${user.id} in game: $gameId with data: $data");
    }
    final response = await client.from('player_state').upsert({
      "dice_state": [data],
      "game_id": gameId,
      "player_id": user.id,
    });
    if (response != null) {
      if (kDebugMode) {
        print(response);
      }
    }
  }

  get uid => user.id;
  get gid => gameId;
  get name => getGameDisplayName(uuid: gameId);

  get document async => await getGameState();
  get snapshots => () {};
}
