import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:schocken/const.dart';
import 'package:schocken/domain/profile_controls.dart';

// import 'package:emojis/emoji.dart'; // to use Emoji collection

class MyGameTrackerWidget extends StatelessWidget {
  final GameStateTracker game;
  final Stream<List<Map<String, dynamic>>> playersStream;

  const MyGameTrackerWidget(
      {super.key, required this.game, required this.playersStream});

  @override
  Widget build(BuildContext context) {
    List<Text>? players;
    return StreamBuilder(
      stream: playersStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }

        if (snapshot.hasError || snapshot.data == null) {
          return const Text("Loading Data...");
        }
        players = [
          for (var player in snapshot.data?.last["players"])
            Text(getUserDisplayName(uuid: player),
                style: const TextStyle(fontSize: 20))
        ];
        if (players?.length == null) {
          return const Text("Loading players...");
        }

        return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                padding: const EdgeInsets.all(10.0),
                itemBuilder: (context, index) => playerStateWidget(
                    context, snapshot.data.last["players"][index],
                    starter: index == 0),
                itemCount: snapshot.data.last["players"].length,
              ))
            ]);
      },
    );
  }

  Widget playerStateWidget(BuildContext context, String playerId,
      {bool starter = false}) {
    final playerStream = game.client
        .from('player_state')
        .stream(primaryKey: ['dice_state']).eq('player_id', playerId);
    return StreamBuilder(
        stream: playerStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }
          if (snapshot.data == null || snapshot.data.length == 0) {
            return Text(
                "Loading Data\nof\n${getUserDisplayName(uuid: playerId)}...");
          }
          var diceState = snapshot.data?.last["dice_state"]?[0];
          if (kDebugMode) {
            print(
                "Dice state: $diceState of player: ${getUserDisplayName(uuid: playerId)}");
          }
          return Padding(
            padding: const EdgeInsets.all(1),
            child: Container(
              // height: double.infinity,
              height: 128,
              width: 96,
              decoration: BoxDecoration(
                border: Border.all(
                  color: (diceState.containsKey('done') && diceState['done'])
                      ? certainColor
                      : uncertainColor,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Stack(alignment: Alignment.center, children: [
                      Text(
                        getUserDisplayName(uuid: playerId),
                        textAlign: TextAlign.center,
                      ),
                      starter && diceState['blind']
                          ? RichText(
                              text: const TextSpan(
                              text: '🕶️', // emoji characters
                              style: TextStyle(
                                fontFamily: 'EmojiOne',
                              ),
                            ))
                          : Container()
                    ]),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      diceState.containsKey('coins')
                          ? Text("Coins:${diceState['coins']}")
                          : const Text("Coins: 0"),
                      (diceState.containsKey('optimusPrime') &&
                              diceState['optimusPrime'])
                          ? const Text(
                              "Opt") // Emoji.all()[Random().nextInt(259)])
                          : Container(),
                    ]),
                    Text("Throws: ${diceState['throws']}"),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          wuerfel(diceState['wuerfel'][0], diceState['done']),
                          wuerfel(diceState['wuerfel'][1], diceState['done']),
                          wuerfel(diceState['wuerfel'][2], diceState['done']),
                        ]),
                  ]),
            ),
          );
        });
  }

  Widget wuerfel(int w, bool done) {
    String iconName = w == 0 ? "?" : "$w";
    return Padding(
      padding: const EdgeInsets.all(3),
      child: Container(
        width: 16,
        height: 16,
        decoration: BoxDecoration(
          border: Border.all(
            color: done ? certainColor : uncertainColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(2),
        ),
        child: Center(child: Image.asset("assets/icons/$iconName.png")),
      ),
    );
  }
}
