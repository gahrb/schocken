import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:schocken/const.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:logo_n_spinner/logo_n_spinner.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:schocken/domain/profile_controls.dart';
import 'package:schocken/layouts/game_page_layout.dart';

class SchockenHomePage extends StatefulWidget {
  final String title;
  final SupabaseClient supabase;
  const SchockenHomePage(
      {super.key, required this.title, required this.supabase});

  @override
  SchockenHomePageState createState() => SchockenHomePageState();
}

class SchockenHomePageState extends State<SchockenHomePage> {
  TextStyle optionStyle =
      const TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  late SchockenUser user;
  late String gameId = "";
  late bool initialized = false;

  void setGameId(String newGameId) {
    setState(() {
      gameId = newGameId;
    });
  }

  void setGame(GameStateTracker game) {
    setState(() {
      game = game;
    });
  }

  void setInitialized() async {
    await user.init();
    setState(() {
      initialized = user.isInitialized;
    });
    user.loginUser();
  }

  @override
  void initState() {
    super.initState();

    // set a new user
    var newUser = SchockenUser(client: widget.supabase);
    setState(() {
      user = newUser;
    });
    setInitialized();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return initialized
              ? Container(
                  decoration: BoxDecoration(
                    border: Border.all(width: 2, color: Colors.blueGrey),
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                  ),
                  padding: const EdgeInsets.all(8),
                  width: min(constraints.maxWidth, 600),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      startGameWidget(),
                      joinGameWidget(),
                      infoFooter(context),
                    ],
                  ))
              : const LogoandSpinner(
                  imageAssets: 'assets/icons/icon_round.png',
                  reverse: true,
                  spinSpeed: Duration(milliseconds: 500),
                );
        },
      ),
    ));
  }

  Widget joinedPlayersWidget({required GameStateTracker game}) {
    final playerStream = game.joinedPlayerStream();
    List<Text>? players;
    return StreamBuilder(
        stream: playerStream,
        builder: (context, snapshot) {
          if (snapshot.hasError || snapshot.data == null) {
            return const Text("Loading Data...");
          }
          players = [
            for (var player in snapshot.data?.last["players"])
              Text(getUserDisplayName(uuid: player),
                  style: const TextStyle(fontSize: 20))
          ];
          if (players?.length == null) {
            return const Text("Loading players...");
          }

          return Container(
              decoration: BoxDecoration(
                border: Border.all(width: 2, color: Colors.blueGrey),
                borderRadius: const BorderRadius.all(Radius.circular(8)),
              ),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                const Text("Joined players:"),
                Column(children: players!),
                LogoandSpinner(
                  imageAssets: 'assets/icons/icon_round.png',
                  reverse: true,
                  spinSpeed: Duration(
                      milliseconds: 500 +
                          500 * log(players!.length.toDouble()) ~/ log(2)),
                )
              ]));
        });
  }

  Widget startGameWidget() {
    // Sets the current user as the group leader and starts the game
    return Container(
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Start a new game',
            style: optionStyle,
          ),
          TextButton(
            onPressed: () {
              GameStateTracker game =
                  GameStateTracker(client: widget.supabase, user: user);
              game.newGame();
              Clipboard.setData(
                ClipboardData(
                  text: game.gameId,
                ),
              );

              // Display game_id in popup alert box
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Send Game ID to friends.'),
                      content:
                          Column(mainAxisSize: MainAxisSize.min, children: [
                        Container(
                          margin: const EdgeInsets.all(5),
                          child: Column(children: [
                            Text(game.name),
                            SelectableText(game.gid,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold)),
                          ]),
                        ),
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 0, 5),
                          child: const Text(
                              "(ID has already been saved to clipboard)"),
                        ),
                        joinedPlayersWidget(game: game),
                      ]),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => MyGameWidget(
                                    game: game,
                                  ),
                                ));
                          },
                          child: const Text(
                              'Click here once everybody has joined.'),
                        ),
                      ],
                    );
                  });
            },
            child: const Text('Start'),
          ),
        ],
      ),
    );
  }

  Widget joinGameWidget() {
    // Textfield to enter the group id
    GameStateTracker game =
        GameStateTracker(client: widget.supabase, user: user, gameId: gameId);
    return Container(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Enter the group id',
            style: optionStyle,
          ),
          TextField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Groupname',
            ),
            onChanged: (value) => setGameId(value),
          ),
          gameId.length > 10
              ? TextButton(
                  onPressed: () {
                    game.joinGame();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MyGameWidget(
                            game: game,
                          ),
                        ));
                  },
                  child: const Text('Join'),
                )
              : const Text(""),
        ],
      ),
    );
  }
}

Widget infoFooter(context) {
  return Container(
      alignment: Alignment.bottomRight,
      child: Text("Build id: ${dotenv.get("BUILD_ID")}"));
}
