import 'dart:math';

import 'package:schocken/domain/game_controls.dart';
import 'package:flutter/material.dart';
import 'package:schocken/domain/game_tracker.dart';
import 'package:schocken/domain/profile_controls.dart';
import 'package:schocken/layouts/main_menu.dart';

class MyGameWidget extends StatefulWidget {
  final GameStateTracker game;
  const MyGameWidget({super.key, required this.game});

  @override
  MyGameWidgetState createState() => MyGameWidgetState();
}

class MyGameWidgetState extends State<MyGameWidget> {
  late Stream<List<Map<String, dynamic>>> playersStream =
      widget.game.joinedPlayerStream();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: Update the stream once the game organizer has started rolling...
    playersStream = widget.game.joinedPlayerStream();
    double pageHeight = MediaQuery.of(context).size.height;
    double pageWidth = min(MediaQuery.of(context).size.width, 600);
    Widget gameTrackerWidget =
        MyGameTrackerWidget(game: widget.game, playersStream: playersStream);
    Widget gameControlWidget =
        MyGameControlsWidget(game: widget.game, playersStream: playersStream);
    return Scaffold(body: Center(
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(width: 2, color: Colors.blueGrey),
            borderRadius: const BorderRadius.all(Radius.circular(8)),
          ),
          padding: const EdgeInsets.all(8),
          width: min(constraints.maxWidth, pageWidth),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              gameBox(gameTrackerWidget, pageHeight * .175),
              gameBox(gameControlWidget, pageHeight * .2),
              infoFooter(context),
            ],
          ),
        );
      }),
    ));
  }

  Widget spacerBox(double height) {
    return Container(
      height: height,
    );
  }

  Widget gameBox(Widget displayedWidget, double height) {
    return SizedBox(
        height: height,
        child: Padding(
            padding: const EdgeInsets.all(3.0), child: displayedWidget));
  }
}
