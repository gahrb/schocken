import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:agora_rtc_engine/agora_rtc_engine.dart';

class MyVideoWidget extends StatefulWidget {
  final String gid;
  final SharedPreferences prefs;
  const MyVideoWidget(
      {required Key key, required this.gid, required this.prefs})
      : super(key: key);

  @override
  MyVideoWidgetState createState() => MyVideoWidgetState();
}

class MyVideoWidgetState extends State<MyVideoWidget> {
  late List<dynamic> users;

  getUsers(String gameId) async {
    // final participants = await supabase.from('game_sessions').select(gameId);
    setState(() {
      // users = participants['users'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsers(widget.gid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          alignment: Alignment.topCenter,
          child: Text("Group: ${widget.gid}"),
        ),
      ),
    );
  }
}

class VideoLayout extends StatefulWidget {
  final String gid;
  final List<dynamic> users;
  final SharedPreferences prefs;
  const VideoLayout(
      {required Key key,
      required this.gid,
      required this.prefs,
      required this.users})
      : super(key: key);
  @override
  VideoLayoutState createState() => VideoLayoutState();
}

class VideoLayoutState extends State<VideoLayout> {
  bool isReady = false;
  double controlSize = 100;
  double aspectRatio = 1.0;
  List<int> _users = <int>[];

  Future<void> _setupCamera() async {
    await Permission.camera.request();
    await Permission.microphone.request();

    // await AgoraRtcEngine.create(AGORA_APP_ID);
    // await AgoraRtcEngine.enableVideo();

    // _addAgoraEventHandlers();

    // await AgoraRtcEngine.enableWebSdkInteroperability(true);
    // await AgoraRtcEngine.setParameters(
    //     '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
    // // TODO: Check out how to set up
    // await AgoraRtcEngine.joinChannel(null, widget.gid, null,
    //     int.parse(widget.prefs.getString('id').codeUnits.sublist(0, 4).join()));
    // // await AgoraRtcEngine.joinChannel(null, "schocken", null, 0);

    setState(() {
      isReady = true;
    });
  }

  // void _addAgoraEventHandlers() {
  // AgoraRtcEngine.onError = (dynamic code) {
  //   setState(() {
  //     final info = 'onError: $code';
  //     log(info);
  //   });
  // };

  // AgoraRtcEngine.onJoinChannelSuccess = (
  //   String channel,
  //   int uid,
  //   int elapsed,
  // ) {
  //   setState(() {
  //     final info = 'onJoinChannel: $channel, uid: $uid';
  //     log(info);
  //   });
  // };

  // AgoraRtcEngine.onLeaveChannel = () {
  //   setState(() {
  //     log('onLeaveChannel');
  //     _users.clear();
  //   });
  // };

  // AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
  //   setState(() {
  //     final info = 'userJoined: $uid';
  //     log(info);
  //     _users.add(uid);
  //   });
  // };

  // AgoraRtcEngine.onUserOffline = (int uid, int reason) {
  //   setState(() {
  //     final info = 'userOffline: $uid';
  //     log(info);
  //     _users.remove(uid);
  //   });
  // };

  // AgoraRtcEngine.onFirstRemoteVideoFrame = (
  //   int uid,
  //   int width,
  //   int height,
  //   int elapsed,
  // ) {
  //   setState(() {
  //     final info = 'firstRemoteVideo: $uid ${width}x $height';
  //     log(info);
  //   });
  // };
  // }

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _setupCamera();
    setUsers();
  }

  setUsers() {
    var otherUsers = widget.users;
    // Only use the other user's stream
    // otherUsers.removeWhere((element) => element == widget.prefs.getString('id'));
    setState(() {
      _users = otherUsers.map((e) {
        return int.parse(e.toString().codeUnits.sublist(0, 4).join());
      }).toList();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    setUsers();

    var userViews = _users.map((uid) {
      return SizedBox(
          width: width / 4,
          height: height * 2 / 11,
          child: Container()); // AgoraRenderWidget(uid));
    }).toList();

    return Container(
        height: height,
        width: width,
        alignment: Alignment.bottomCenter,
        child: Stack(
            alignment: Alignment.center,
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            // crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
                  Container(
                    width: width / 4,
                    height: height * 2 / 11,
                    padding: const EdgeInsets.all(8.0),
                    child: isReady
                        ? Container() // AgoraRenderWidget(0, local: true, preview: true)
                        : const Center(child: CircularProgressIndicator()),
                  )
                ] +
                userViews));
  }
}
