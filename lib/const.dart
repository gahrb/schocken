import 'package:flutter/material.dart';
import 'dart:math';

const agoraAppId = '50f40dc8870d42679c086215a7319c3d';

const themeColor = Colors.lightBlueAccent;
const primaryColor = Color(0xff203152);
const greyColor = Color(0xffaeaeae);
const greyColor2 = Color(0xffE8E8E8);
const greyColor3 = Color(0xffEFEFEF);
const alertRed = Colors.redAccent;
const trueColor = Colors.blueGrey;
const falseColor = alertRed;
const uncertainColor = Colors.orangeAccent;
const certainColor = Colors.green;

final animalAdjectives = [
  'Nimble',
  'Clever',
  'Wise',
  'Brave',
  'Mighty',
  'Gentle',
  'Fierce',
  'Swift',
  'Sneaky',
  'Loyal',
  'Silly',
  'Curious',
  'Elegant',
  'Graceful',
  'Majestic',
  'Playful',
  'Powerful',
  'Radiant',
  'Regal',
  'Savage',
  'Spirited',
  'Stately',
  'Tranquil',
  'Vigilant',
  'Witty',
];
final animalSpecies = [
  'Kangaroo',
  'Lion',
  'Tiger',
  'Bear',
  'Wolf',
  'Fox',
  'Horse',
  'Giraffe',
  'Elephant',
  'Rhino',
  'Leopard',
  'Cheetah',
  'Gorilla',
  'Zebra',
  'Hippopotamus',
  'Crocodile',
  'Puma',
  'Jaguar',
  'Panther',
  'Lynx',
  'Ocelot',
  'Mongoose',
  'Hyena',
  'Badger',
  'Raccoon',
];

final oasisAdjectives = [
  'Flourishing',
  'Lush',
  'Verdant',
  'Bountiful',
  'Fertile',
  'Abundant',
  'Luxuriant',
  'Green',
  'Tropical',
  'Exotic',
  'Oasis-like',
  'Paradisiacal',
  'Edenic',
  'Heavenly',
  'Idyllic',
  'Serenity',
  'Tranquil',
  'Peaceful',
  'Harmonious',
  'Balanced',
  'Calm',
  'Relaxing',
  'Rejuvenating',
  'Refreshing',
  'Revitalizing',
];

final meetingPoints = [
  "Forest Glen",
  "Meadow Haven",
  "Sunflower Field",
  "Mountain Peak",
  "Riverbank Oasis",
  "Coral Reef Hideaway",
  "Savannah Retreat",
  "Treetop Haven",
  "Desert Oasis",
  "Arctic Ice Floe",
  "Jungle Canopy",
  "Coastal Cliff",
  "Alpine Meadow",
  "Wetland Marsh",
  "Bamboo Grove",
  "Volcanic Crater",
  "Prairie Grasslands",
  "Cavern Sanctuary",
  "Mossy Glen",
  "Secluded Beach",
  "Lush Valley",
  "Island Archipelago",
  "Deep Sea Trench",
  "Cactus Canyon",
  "Enchanted Garden",
  "Rainforest Canopy",
];
String getUserDisplayName({required String uuid}) {
  final random = Random(uuid.hashCode);
  final adjective = animalAdjectives[random.nextInt(animalAdjectives.length)];
  final species = animalSpecies[random.nextInt(animalSpecies.length)];
  return '$adjective $species';
}

String getGameDisplayName({required String uuid}) {
  final random = Random(uuid.hashCode);
  final adjective = oasisAdjectives[random.nextInt(oasisAdjectives.length)];
  final state = meetingPoints[random.nextInt(meetingPoints.length)];
  return '$adjective $state';
}
