import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:schocken/layouts/main_menu.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:mockito/mockito.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class MockSupabaseClient extends Mock implements SupabaseClient {}

Future<void> main() async {
  try {
    await dotenv.load(fileName: ".env");
  } catch (e) {
    if (kDebugMode) {
      print("Not local deployment. No env file available: $e");
    }
  }
  await Supabase.initialize(
    url: 'https://${dotenv.get('SUPABASE_URL_PREFIX')}.supabase.co',
    anonKey: dotenv.get('SUPABASE_ANON_KEY'),
  );
  final supabase = Supabase.instance.client;
  runApp(SchockenApp(supabase: supabase));
}

class SchockenApp extends StatelessWidget {
  final SupabaseClient supabase;
  const SchockenApp({super.key, required this.supabase});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Schocken',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'FixedSys',
      ),
      home: SchockenHomePage(
        title: 'Schocken App',
        supabase: supabase,
      ),
    );
  }
}
