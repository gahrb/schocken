import 'package:flutter_test/flutter_test.dart';
import 'package:schocken/domain/schocken.dart';

void main() {
  test('getCoins returns correct number of coins for different scores', () {
    expect(getCoins(40000), 13);
    expect(getCoins(36000), 6);
    expect(getCoins(35000), 5);
    expect(getCoins(34000), 4);
    expect(getCoins(33000), 3);
    expect(getCoins(32000), 2);
    expect(getCoins(20000), 3);
    expect(getCoins(10000), 2);
    expect(getCoins(0), 1);
  });
  test('getWinnerAndLoser returns correct winner and loser', () {
    // Test case 1: Two players with different scores
    Map<String, int> throwValues = {'player1': 100, 'player2': 50};
    Map<String, String> result = getWinnerAndLoser(throwValues: throwValues);
    expect(result['winner'], 'player1');
    expect(result['loser'], 'player2');

    // Test case 2: Two players with the same score (Mitte-shit)
    throwValues = {'player1': 100, 'player2': 100};
    result = getWinnerAndLoser(throwValues: throwValues);
    expect(result['winner'], 'player1');
    expect(result['loser'], 'player2');

    // Test case 3: Three players with different scores
    throwValues = {'player1': 100, 'player2': 50, 'player3': 75};
    result = getWinnerAndLoser(throwValues: throwValues);
    expect(result['winner'], 'player1');
    expect(result['loser'], 'player2');
  });
  test(
      'calcValue returns correct score for different wuerfel, muetze, and throws',
      () {
    // Test case 1: Three 1s
    expect(calcValue(wuerfel: [1, 1, 1], muetze: false, throws: 1), 40004);
    // Test case 2: Two 1s and a 2
    expect(calcValue(wuerfel: [1, 1, 2], muetze: false, throws: 1), 32004);
    // Test case 2b: Two 1s and a 2, different order
    expect(calcValue(wuerfel: [1, 2, 1], muetze: false, throws: 3),
        calcValue(wuerfel: [2, 1, 1], muetze: false, throws: 3));
    // Test case 3: Three consecutive numbers with muetze
    expect(calcValue(wuerfel: [2, 3, 4], muetze: true, throws: 1), 12005);
    // Test case 4: Random wuerfel without muetze
    expect(calcValue(wuerfel: [4, 5, 6], muetze: false, throws: 2), 6542);
    // Test case 5: Drilling
    expect(calcValue(wuerfel: [4, 4, 4], muetze: true, throws: 3), 24001);

    // TODO: Add edge cases and compare them (e.g., muetze vs no muetze)
  });
}
